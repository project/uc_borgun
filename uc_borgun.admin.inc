<?php

/*******************************************************************************
 * Callback Functions, Forms, and Tables
 ******************************************************************************/

// Handles the Borgun payment method.
function uc_payment_method_borgun($op, &$arg1) {
  switch ($op) {
    case 'cart-details':
      return t('Continue with checkout to be redirected to Borgun SecurePay for payment.');
    case 'settings':
      $form = array();
      $form['#description'] = t('Note: To process test transactions, you must adjust your account settings through your Borgun account manager.');
      $form['uc_borgun_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Borgun Connect URL'),
        '#default_value' => variable_get('uc_borgun_url', 'https://securepay.borgun.is/securepay/default.aspx'),
        '#size' => 70,
        '#maxlength' => 180,
        '#description' => t('URL to connect to the Borgun service'),
        '#required' => TRUE,
      );
      $form['uc_borgun_buyername'] = array(    
        '#type' => 'textfield',
        '#title' => t('Buyer Name'),
        '#default_value' => variable_get('uc_borgun_buyername', ''),
        '#size' => 25,
        '#maxlength' => 25,
        '#required' => FALSE,
      );
      $form['uc_borgun_merchantid'] = array(    
        '#type' => 'textfield',
        '#title' => t('Merchantid'),
        '#default_value' => variable_get('uc_borgun_merchantid', ''),
        '#size' => 25,
        '#maxlength' => 25,
        '#description' => t('MerchantId issued by Borgun that identifies the merchant.'),
        '#required' => TRUE,
      );
      $form['uc_borgun_secure_code'] = array(    
        '#type' => 'textfield',
        '#title' => t('Secure code'),
        '#default_value' => variable_get('uc_borgun_secure_code', ''),
        '#size' => 25,
        '#maxlength' => 25,
        '#description' => t('Secret key is issued by Borgun and known only to merchant and Borgun.'),
        '#required' => TRUE,
      );
      $form['uc_borgun_paymentgatewayid'] = array(    
        '#type' => 'textfield',
        '#title' => t('Payment Gateway Id'),
        '#default_value' => variable_get('uc_borgun_paymentgatewayid', ''),
        '#size' => 25,
        '#maxlength' => 25,
        '#description' => t('Payment Gateway Id issued by Borgun that identifies the payment method.'),
        '#required' => TRUE
      );
      $form['uc_borgun_merchantemail'] = array(    
        '#type' => 'textfield',
        '#title' => t('Merchant Email'),
        '#default_value' => variable_get('uc_borgun_merchantemail', ''),
        '#size' => 25,
        '#maxlength' => 25,
        '#description' => t('Optional. If present, an e-mail is sent to the merchant upon successful payment. The message contains information about the merchant and buyer, along with the contents of the shopping.'),
        '#required' => FALSE,
      );
      $form['uc_borgun_currency'] = array(
        '#type' => 'radios',
        '#title' => t('Currency'),
        '#default_value' => variable_get('uc_borgun_currency', 'ISK'),
        '#options' => array(
          'GBP' => t('GBP'), 
          'USD' => t('USD'),
          'EUR' => t('EUR'),
          'ISK' => t('ISK'),
        ),
      );
      $form[$variable]['uc_borgun_language'] = array(
        '#type' => 'radios',
        '#title' => t('Language'),
        '#default_value' => variable_get('uc_borgun_language', 'ISL'),
        '#options' => array(
          'ISL' => t('Icelandic'), 
          'EN' => t('English'),
        ),
      );
      return $form;
  }
}

