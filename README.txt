Borgun Iceland Payment 
=========================
This is a Borgun payment gateway for Ubercart, which allows Ubercart to accept credit and debet card payments via Borgun Iceland Secure Payment (http://Borgun.is).

Author
=========================
Juan Vizcarrondo (http://drupal.org/user/352652) 

Install
=========================
1) Simply drop this module's directory into your modules directory.
2) Install dependences (uc_payment).
3) Install via admin/build/modules.

Payment Process
=========================
The process steps between webshop and borgun are as follows:
1) Process initiation, webshop redirects buyer to Borgun‘s payment page with cart and payment information. The checkout is completed will redirect the customer to the Borgun gateway.
2) Buyer can take two actions.
  a) Push cancel button and be returned to the cancelurl supplied by webshop.
  b) Supply creditcard information and finalize payment.
3) As soon as payment is concluded the Borgun server sends a payment confirmation to the webshop with the success url supplied by webshop.
4) Receipt of transaction the displayed to the buyer.
5) After pressing the "Back to shop" button the buyer is redirected to to the success url supplied by webshop.

Borgun settings
=========================
To enter your Borgun Merchant account details go to admin/store/settings/payment/edit/gateways:
• Borgun Connect URL: Path of Borgun gateway, use https://test.borgun.is/SecurePay/default.aspx to use Test environment and https://securepay.borgun.is/securepay/default.aspx to production environment.
• Buyer Name: Buyers name. If left empty then the buyer can insert it on the payment page,
• Merchantid: MerchantId issued by Borgun that identifies the merchant.
• Secure code: Secret code is issued by Borgun and known only to merchant and Borgun.
• Payment Gateway Id: Payment Gateway Id issued by Borgun that identifies the payment method.

Optionals:
• Merchant Email: Optional. If present, an e-mail is sent to the merchant upon successful payment. The message contains information about the merchant and buyer, along with the contents of the shopping.
• Currency: Currency code, allowed values are GBP, USD, EUR and ISK. Default value is ISK.
• Language: Language on webpages displayed to users. Supported langages are icelandic (IS) and english (EN).
