<?php
// Handles the response sent by borgun.
function uc_borgun_post($order_id = 0) {
  global $user;
  $uc_borgun_step = strtoupper($_POST['step']);
  $uc_borgun_status = strtoupper($_POST['status']);
  switch ($uc_borgun_step) {
    case 'PAYMENT':
      $uc_borgun_authorizationcode = $_POST['authorizationcode'];
      if ($uc_borgun_status == "OK") {
        $uc_borgun_orderid = intval($_POST['orderid']);
        //Check order
        $order = uc_order_load($uc_borgun_orderid);
        if ($order->order_id) {
          //Check hash
          $uc_borgun_orderhash = $_POST['orderhash'];
          $uc_borgun_amount = $order->order_total;
          $uc_borgun_amount = number_format(round($order->order_total, 2),2,'.','');
          $uc_borgun_secure_code = variable_get('uc_borgun_secure_code', '');
          $uc_borgun_orderhash_check = $order->order_id . $uc_borgun_amount . $uc_borgun_secure_code;
          $uc_borgun_orderhash_check = md5($uc_borgun_orderhash_check);
          if ($uc_borgun_orderhash == $uc_borgun_orderhash_check) {
            //Borgun feedback success
            $context = array(
              'revision' => 'formatted-original',
              'type' => 'amount',
            );
            $options = array(
              'sign' => FALSE,
            );
            $uc_borgun_creditcardnumber = check_plain($_POST['creditcardnumber']);
            $comment = t('Borgun transaction ID: @authorizationcode, Credit Card Number: @creditcardnumber', array(
              '@authorizationcode' => $uc_borgun_authorizationcode,
              '@creditcardnumber' => $uc_borgun_creditcardnumber,
              )
            );
            uc_payment_enter(
              $order->order_id, 
              'borgun', 
              uc_price($uc_borgun_amount, $context, $options), 
              $order->uid, 
              NULL, 
              $comment
            );
            uc_cart_complete_sale($order);
            uc_order_comment_save(
              $order->order_id, 
              0, 
              t('Payment of @amount submitted through Borgun.', array(
                '@amount' => uc_price($uc_borgun_amount, $context, $options),
                )
              ), 
              'order', 
              'payment_received'
            );
            uc_order_comment_save(
              $order->order_id, 
              0, 
              t('Borgun reported a payment of @amount.', 
                array(
                  '@amount' => uc_price($uc_borgun_amount, $context, $options),
                )
              )
            );
            //Notify borgun payment was received
            uc_borgun_checkout_notification_acknowledgement("ACCEPTED");
          }
          else {
            //Notify Hash error
            watchdog('uc_borgun', 'Borgun transaction @authorizationcode failed hash verification.', array('@authorizationcode' => $uc_borgun_authorizationcode), WATCHDOG_ERROR);
            uc_order_comment_save($order->order_id, 0, t('An Borgun transaction @authorizationcode failed hash verification for this order.', array('@authorizationcode' => $uc_borgun_authorizationcode)), 'admin');
          }
        }
        else {
          //Notify order not found
          watchdog('uc_borgun', 'Borgun transaction @authorizationcode failed orderid return.', array('@authorizationcode' => $uc_borgun_authorizationcode), WATCHDOG_ERROR);
        }
      }
      else {
        //Notify status not ok
        watchdog('uc_borgun', 'Borgun transaction @authorizationcode failed status verification.', array('@authorizationcode' => $uc_borgun_authorizationcode), WATCHDOG_ERROR);
      }
      drupal_goto('cart/checkout/review');
      break; 
    case 'CONFIRMATION':
      $uc_borgun_authorizationcode = $_POST['authorizationcode'];
      if ($uc_borgun_status == "OK") {
        $uc_borgun_orderid = intval($_POST['orderid']);
        //Check order
        $order = uc_order_load($uc_borgun_orderid);
        if ($order->order_id) {
          //Check hash
          $uc_borgun_orderhash = $_POST['orderhash'];
          $uc_borgun_amount = $order->order_total;
          $uc_borgun_amount = number_format(round($order->order_total, 2),2,'.','');
          $uc_borgun_secure_code = variable_get('uc_borgun_secure_code', '');
          $uc_borgun_orderhash_check = $order->order_id . $uc_borgun_amount . $uc_borgun_secure_code;
          $uc_borgun_orderhash_check = md5($uc_borgun_orderhash_check);
          if ($uc_borgun_orderhash == $uc_borgun_orderhash_check) {
            //order not completed
            if ($order->order_status != "payment_received" & $order->order_status != "completed") {
              //Borgun feedback success
              $context = array(
                'revision' => 'formatted-original',
                'type' => 'amount',
              );
              $options = array(
                'sign' => FALSE,
              );
              $uc_borgun_creditcardnumber = check_plain($_POST['creditcardnumber']);
              $comment = t('Borgun transaction ID: @authorizationcode, Credit Card Number: @creditcardnumber in Confirmation step.', array(
                '@authorizationcode' => $uc_borgun_authorizationcode,
                '@creditcardnumber' => $uc_borgun_creditcardnumber,
                )
              );
              uc_payment_enter(
                $order->order_id, 
                'borgun', 
                uc_price($uc_borgun_amount, $context, $options), 
                $order->uid, 
                NULL, 
                $comment
              );
              uc_cart_complete_sale($order);
              uc_order_comment_save(
                $order->order_id, 
                0, 
                t('Payment of @amount submitted through Borgun in Confirmation step.', array(
                  '@amount' => uc_price($uc_borgun_amount, $context, $options),
                  )
                ), 
                'order', 
                'payment_received'
              );
              uc_order_comment_save(
                $order->order_id, 
                0, 
                t('Borgun reported a payment of @amount in Confirmation step.', 
                  array(
                    '@amount' => uc_price($uc_borgun_amount, $context, $options),
                  )
                )
              );
            }
            return uc_borgun_display_completed_order($order);
          }
          else {
            //Notify Hash error
            watchdog('uc_borgun', 'Borgun transaction @authorizationcode failed hash verification.', array('@authorizationcode' => $uc_borgun_authorizationcode), WATCHDOG_ERROR);
            uc_order_comment_save($order->order_id, 0, t('An Borgun transaction failed hash verification for this order.'), 'admin');
          }
        }
        else {
          //Notify order not found
          watchdog('uc_borgun', 'Borgun transaction @authorizationcode failed orderid return.', array('@authorizationcode' => $uc_borgun_authorizationcode), WATCHDOG_ERROR);
        }
      }
      else {
        //Notify status not ok
        watchdog('uc_borgun', 'Borgun transaction @authorizationcode failed status verification.', array('@authorizationcode' => $uc_borgun_authorizationcode), WATCHDOG_ERROR);
      }
      drupal_goto('cart/checkout/review');
      break;
  }
  switch ($uc_borgun_status) { 
    case 'CANCEL':
      $order_id = intval($order_id);
      //Check order
      $order = uc_order_load($order_id);
      if ($order->order_id) {
        $context = array(
          'revision' => 'formatted-original',
          'type' => 'amount',
        );
        $options = array(
          'sign' => FALSE,
        );
        uc_order_comment_save($order->order_id, 0, t('Payment of @amount Cancel through Burgon.', array('@amount' => uc_price($order->order_total, $context, $options),)),'admin');
      }
      drupal_goto('cart/checkout/review');
      break;
    case 'ERROR':
      drupal_set_message(t("An error occurred while trying to make payment"));
      $order_id = intval($order_id);
      //Check order
      $order = uc_order_load($order_id);
      $uc_borgun_errordetail = check_plain($_POST['errordetail']);
      $uc_borgun_errordescription = check_plain($_POST['errordescription']);
      $uc_borgun_errorcode = check_plain($_POST['errorcode']);
      if ($order->order_id) {
        $context = array(
          'revision' => 'formatted-original',
          'type' => 'amount',
        );
        $options = array(
          'sign' => FALSE,
        );
        uc_order_comment_save($order->order_id, 0, t('Payment of @amount error through Burgon code: @code (@description), detail: @detail.', array('@amount' => uc_price($order->order_total, $context, $options), '@code' => $uc_borgun_errorcode, '@description' => $uc_borgun_errordescription, '@detail' => $uc_borgun_errordetail)),'admin');
      }
      watchdog('uc_borgun', 'Borgun transaction report error code: @code (@description), detail: @detail.', array('@code' => $uc_borgun_errorcode, '@description' => $uc_borgun_errordescription, '@detail' => $uc_borgun_errordetail), WATCHDOG_ERROR);
      drupal_goto('cart/checkout/review');
      break;
  }
  drupal_goto('cart/checkout/review');
}
